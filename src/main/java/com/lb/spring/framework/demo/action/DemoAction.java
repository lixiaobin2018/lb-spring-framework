package com.lb.spring.framework.demo.action;


import com.lb.spring.framework.annotation.LBAutowired;
import com.lb.spring.framework.annotation.LBController;
import com.lb.spring.framework.annotation.LBRequestMapping;
import com.lb.spring.framework.annotation.LBRequestParam;
import com.lb.spring.framework.demo.service.IDemoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


//虽然，用法一样，但是没有功能
@LBController
@LBRequestMapping("/demo")
public class DemoAction {

  	@LBAutowired
    private IDemoService demoService;

	@LBRequestMapping("/query")
	public void query(HttpServletRequest req, HttpServletResponse resp,
                      @LBRequestParam("name") String name){
//		String result = demoService.get(name);
		String result = "My name is " + name;
		try {
			resp.getWriter().write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@LBRequestMapping("/add")
	public void add(HttpServletRequest req, HttpServletResponse resp,
                    @LBRequestParam("a") Integer a, @LBRequestParam("b") Integer b){
		try {
			resp.getWriter().write(a + "+" + b + "=" + (a + b));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@LBRequestMapping("/remove")
	public void remove(HttpServletRequest req, HttpServletResponse resp,
                       @LBRequestParam("id") Integer id){
	}

}
