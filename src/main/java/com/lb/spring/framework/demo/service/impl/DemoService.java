package com.lb.spring.framework.demo.service.impl;


import com.lb.spring.framework.annotation.LBService;
import com.lb.spring.framework.demo.service.IDemoService;

/**
 * 核心业务逻辑
 */
@LBService
public class DemoService implements IDemoService {

    @Override
	public String get(String name) {
		return "My name is " + name;
	}

}
