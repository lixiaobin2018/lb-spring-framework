package com.lb.spring.framework.demo.service;


public interface IDemoService {

	String get(String name);

}
